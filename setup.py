from setuptools import setup

setup(
    name='altigen',
    version='0.2',
    packages=['altigen'],
    url='',
    license='MIT',
    author='Phillip Stromberg',
    author_email='phillip@4stromberg.com',
    description='A Python library for interacting with an Altigen database',
    install_requires=[
        'adodbapi',
        'pywin32',
    ],
    scripts=['scripts/get_extensions.py', 'scripts/get_calls.py']
)
