#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
altigen

Created by phillip on 9/30/2016
"""

from __future__ import unicode_literals

import altigen
from argparse import ArgumentParser


if __name__ == '__main__':
    parser = ArgumentParser('get_extensions.py')
    parser.add_argument('filename',
                        help=r"The path to the Altigen Internal.mdb file. Usually stored in "
                             r"C:\AltiDB\InternalDB\Internal.mdb on your PBX.")
    parser.add_argument('--min-ext', type=int, default=0,
                        help='The minimum extension number to show.')
    parser.add_argument('--max-ext', type=int, default=9999,
                        help='The maximum extension number to show.')
    args = parser.parse_args()

    filename = args.filename
    altidb = altigen.open(filename)
    for ext in altidb.get_extensions(args.min_ext, args.max_ext):
        print(ext)
