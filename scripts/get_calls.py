#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Given the path to the Internal.mdb file from an Altigen MAX1000-R PBX, displays the day's
call logs and a summary of talk time and hold time.

Use the optional --date flag to specify a day to get records for in the format YYYY-MM-DD


Created by phillip on 9/30/2016
"""

from __future__ import unicode_literals

import altigen
from argparse import ArgumentParser
from datetime import datetime


if __name__ == '__main__':
    parser = ArgumentParser('get_calls.py')
    parser.add_argument('filename',
                        help=r"The path to the Altigen Internal.mdb file. Usually stored in "
                             r"C:\AltiDB\InternalDB\Internal.mdb on your PBX.")
    parser.add_argument('--date', default=None,
                        help='The day you want to get phone records for in the format YYYY-MM-DD.')
    args = parser.parse_args()

    filename = args.filename
    altidb = altigen.open(filename)
    day = args.date
    if day:
        day = datetime.strptime(args.date, '%Y-%m-%d').date()
    call_log = altidb.get_day_call_log(day)
    human_date = day.strftime("%A, %B %d, %Y")

    if not call_log:
        print("No calls were placed on {}".format(human_date))
        exit(2)
    print("Call Log for {}:\r\n".format(day.strftime(human_date)))
    for rec in call_log:
        print(rec)

    print("\r\n\r\nTop 10 Talkers:\r\n")
    print("{: <12}\t{: <20}\t{: <15}".format('Number', 'Name', 'Minutes Talking'))
    print("=" * 62)
    for r in altidb.get_top_talkers(day, limit=10):
        r['minutes'] = r['seconds'] / 60.0
        print('{number: <12}\t{name: <20}\t{minutes: .2f}'.format(**r))

    print("\r\n\r\nTop 10 Longest Waited on Hold:\r\n")
    print("{: <12}\t{: <20}\t{: <15}".format('Number', 'Name', 'Seconds Holding'))
    print("=" * 62)
    for r in altidb.get_top_holders(day, limit=10):
        print('{number: <12}\t{name: <20}\t{seconds}'.format(**r))
