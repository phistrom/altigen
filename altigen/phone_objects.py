# -*- coding: utf-8 -*-
"""
altigen

Created by phillip on 9/30/2016
"""
from __future__ import unicode_literals

from datetime import datetime


class Extension(object):
    """
    Represents an extension in the phone system. Has an extension number and the person's first and last names.
    """
    def __init__(self, number=None, first_name=None, last_name=None):
        self.number = number
        self.first_name = first_name
        self.last_name = last_name

    def __lt__(self, other):
        try:
            return self.number < other.number
        except:
            return False

    def __eq__(self, other):
        try:
            return self.number == other.number and \
                   self.first_name == other.first_name and \
                   self.last_name == other.last_name
        except:
            return False

    def __str__(self):
        return "x%d %s" % (self.number, ' '.join([self.first_name, self.last_name]).strip())


class PhoneCall(object):
    """
    Represents a phone call in the call log. This comes from the table "CDRMAIN" in the Internal.mdb
    """

    DB_TO_FIELD_MAPPING = [
        ('start_time', 'starttime'),
        ('end_time', 'endtime'),
        ('caller_number', 'callernum'),
        ('caller_name', 'callername'),
        ('target_number', 'targetnum'),
        ('target_name', 'targetname'),
        ('direction', 'direction'),
        ('dnis', 'dnis'),
        ('hold_duration', 'holdduration'),
        ('talk_duration', 'talkduration'),
    ]

    INBOUND = 1  # in the Internal.mdb, a 1 in the "Direction" column means from outside caller
    OUTBOUND = 2  # in the Internal.mdb, a 2 in the "Direction" column means from an internal extension

    def __init__(self, caller_number=None, caller_name=None, target_number=None, target_name=None, start_time=None,
                 end_time=None, direction=None, dnis=None, hold_duration=None, talk_duration=None):
        self._start_time = None
        self.caller_number = caller_number
        self.caller_name = caller_name
        self.target_number = target_number
        self.target_name = target_name
        self.start_time = start_time
        self.end_time = end_time
        self.direction = direction
        self.dnis = dnis
        self.hold_duration = hold_duration
        self.talk_duration = talk_duration

    def __str__(self):
        inbound = self.direction == 1

        values = {
            'ts': '' if not self.start_time else self.start_time.strftime("%Y-%m-%d %I:%M:%S %p"),
            'direct': "<--" if inbound else "-->",
            'internalname': self.target_name if inbound else self.caller_name,
            'internalnum': self.target_number if inbound else self.caller_number,
            'externalname': self.caller_name if inbound else self.target_name,
            'externalnum': self.caller_number if inbound else self.target_number,
            'talkminutes': self.talk_duration / 60.0,
            'holdseconds': self.hold_duration,
        }

        return "[{ts}] {internalname: <20}\t{internalnum: <4}\t{direct: ^4}\t" \
               "{externalname: <20}\t{externalnum: <12}\t" \
               "(talked {talkminutes:0.2f} minutes; on hold for {holdseconds} seconds)".format(**values)

    def __unicode__(self):
        return str(self).decode('utf-8')

    @property
    def start_time(self):
        return self._start_time

    @start_time.setter
    def start_time(self, value):
        try:
            epoch_time = int(value)
            value = datetime.fromtimestamp(epoch_time)
        except (TypeError, ValueError):
            pass
        try:
            if value is not None:
                value.year
        except AttributeError:
            raise ValueError("Expected an integer, datetime object, or None for start_time. Got '%s'" % value)
        self._start_time = value

    @classmethod
    def _from_database_row(cls, column_names, row):
        rowdict = {}
        for name, idx in column_names.items():
            rowdict[name] = row[idx]
        newrecord = cls()
        for field, dbcolname in cls.DB_TO_FIELD_MAPPING:
            setattr(newrecord, field, rowdict.get(dbcolname))
        return newrecord
