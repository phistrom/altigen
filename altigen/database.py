#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
altigen

Created by phillip on 9/30/2016
"""
from __future__ import unicode_literals

import adodbapi as db
from datetime import date
from .phone_objects import Extension, PhoneCall


class AltigenConnection(object):
    CONNECTION_STRING = "Provider=%(provider)s;Data Source=%(filename)s"
    PROVIDER_SWITCH = ('provider', 'Microsoft.ACE.OLEDB.12.0', "Microsoft.Jet.OLEDB.4.0")

    def __init__(self, file_path):
        self._file_path = file_path
        self._conn = None

    def __enter__(self):
        kwargs = {'filename': self._file_path}
        self._conn = db.connect(self.CONNECTION_STRING, kwargs, macro_is64bit=self.PROVIDER_SWITCH)
        cursor = self._conn.cursor()
        return cursor

    def __exit__(self, exc_type, exc_val, exc_tb):
        try:
            self._conn.close()
        except:
            pass
        self._conn = None


class AltigenDatabase(object):
    def __init__(self, path_to_mdb):
        self.path_to_mdb = path_to_mdb
        self._conn = AltigenConnection(path_to_mdb)

    def get_top_talkers(self, day=None, limit=10):
        return self._get_top_callers('talk', day, limit)

    def get_top_holders(self, day=None, limit=10):
        return self._get_top_callers('hold', day, limit)

    def _get_top_callers(self, type, day=None, limit=10):
        if day is None:
            day = date.today()

        if type == 'talk':
            colname = 'c.TalkDuration'
        elif type == 'hold':
            colname = 'c.HoldDuration'
        else:
            raise ValueError("%s is not a type of top caller I can get records for. Did you mean 'talk' or 'hold'?" % type)

        rows = []
        with self._conn as cursor:
            if limit > 0:
                cursor.execute(
                    """SELECT TOP %(limit)d c.CallerNum, c.CallerName, SUM(%(colname)s) AS Duration
                       FROM CDRMAIN c
                       WHERE LocalDay=?
                       GROUP BY c.CallerNum, c.CallerName
                       ORDER BY SUM(%(colname)s) DESC""" % {'limit': limit, 'colname': colname},
                    (day.strftime('%Y%m%d'),)
                )
                for r in cursor.fetchall():
                    rec = {
                        'number': r[0],
                        'name': r[1],
                        'seconds': int(r[2])
                    }
                    rows.append(rec)
            return rows

    def get_day_call_log(self, day=None):
        if day is None:
            day = date.today()
        records = []
        with self._conn as cursor:
            cursor.execute(
                """SELECT *
                   FROM CDRMAIN c
                   WHERE c.LocalDay=?
                   ORDER BY c.StartTime""", (day.strftime('%Y%m%d'),)
            )
            colnames = cursor.columnNames
            for row in cursor.fetchall():
                rec = PhoneCall._from_database_row(colnames, row)
                records.append(rec)

        return records

    def get_extensions(self, min=0, max=9999):
        extensions = []
        with self._conn as cursor:
            cursor.execute(
                """SELECT e.ExtNum, e.FirstName, e.LastName
                   FROM EXTINFORMATION e
                   WHERE e.ExtNum >= ? AND e.ExtNum <= ? AND e.RevisionID = 0
                   ORDER BY e.ExtNum, e.RevisionID""", (min, max)
            )
            for row in cursor.fetchall():
                ext = Extension(number=row[0], first_name=row[1], last_name=row[2])
                extensions.append(ext)
        return extensions
