# Altigen Library for Python

Just a Python library for poking around at Altigen's Internal.mdb file.

## Setup
### Requires
  - [adodbapi] - can open Access database files
  - [pywin32] - required for adodbapi

You also probably need the [Microsoft ODBC driver for Access].

### Install
Run `python setup.py install`.

## Usage
`get_extensions.py \\path\to\your\AltiDb\Internal.mdb`

Run from the command line to get the current list of extensions in the format `x123 FirstName LastName`.

You can also specify the arguments `--max-ext` and `--min-ext` to specify a range of extension numbers
you're interested in.

More features planned. I created this library on a whim.

License
----

MIT

   [pywin32]: <https://sourceforge.net/projects/pywin32/files/pywin32/>
   [adodbapi]: <http://adodbapi.sourceforge.net/>
   [Microsoft ODBC driver for Access]: <https://www.microsoft.com/en-us/download/details.aspx?id=13255>
